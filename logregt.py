import numpy as np
import matplotlib.pyplot as plt


x=np.genfromtxt("proj_train")
y=x[:,0]
x=x[:,1:]
N=x.shape[0]
d=x.shape[1]
x=np.hstack((np.ones((N,1)),x))

xt=np.genfromtxt("proj_test")
Nt=xt.shape[0]
xt=np.hstack((np.ones((Nt,1)),xt))

ita=0.001
T=10000

w=np.zeros(d+1)

ty=y
y=y.reshape(-1,1)

m1=[]
m2=[]
for i in range(x.shape[1]):
	m1.append(np.mean(x[:,i][~np.isnan(x[:,i])] ) )

for i in range(xt.shape[1]):
	m2.append(np.mean(xt[:,i][~np.isnan(xt[:,i])] ) )

m1=np.array(m1)
m2=np.array(m2)
m1=(m1*N+m2*Nt)/(N+Nt)
#y*x
#ein=[np.sum((np.sum((w*x),1)>=0)!=(ty==1))/float(N)]
#eout=[np.sum((np.sum((w*xt),1)>=0)!=(yt==1))/float(Nt)]
#ein=[]
#eout=[]
for i in range(x.shape[0]):
	for j in range(x.shape[1]):
		if np.isnan(x[i][j]):
			x[i][j]=m1[j]

for i in range(xt.shape[0]):
	for j in range(xt.shape[1]):
		if np.isnan(xt[i][j]):
			xt[i][j]=m1[j]

import random

wq=[]
for t in range(T):
	#z=(y*x)/(np.exp(np.sum(w*x,1).reshape(-1,1)*y)+1)
	i=np.random.randint(N)
	#i=random.randint(0,N-1)
	w=w+ita*(y[i]*x[i])/(np.exp(np.sum(w*x[i])*y[i])+1)
	#print ita*(y[i]*x[i])/(np.exp(np.sum(w*x[i])*y[i])+1)
	#print w
	#wq.append(w)
	#ein.append(np.sum((np.sum((w*x),1)>=0)!=(ty==1))/float(N))
	#eout.append(np.sum((np.sum((w*xt),1)>=0)!=(yt==1))/float(Nt))

auc(y.T[0],np.dot(x,w))

"""
array([-0.04400025, -0.00302969, -0.00355182, -0.00656243, -0.01267495,
	       -0.01382955, -0.01208027, -0.00754828, -0.01310838, -0.00432793,
			        -0.00676859, -0.00376483, -0.00323995, -0.00599451, -0.0213746 ,
					         -0.03374485, -0.00247967, -0.1142133 , -0.11504026, -0.11816283,
								       -0.09956381, -0.12586807, -0.12055005,  0.0165917 , -0.04485132,
										        -0.00593784,  0.00417739,  0.02563121,  0.02090392,  0.06333762,
												         -0.01781532, -0.16831835, -0.01698512, -0.01544786, -0.00499859,
															       -0.00434216, -0.00455384, -0.00436873, -0.0024904 , -0.03656013,
																	        -0.07764465, -0.07355176, -0.07270319, -0.09116024, -0.09447387,
																			         -0.09444197, -0.00357055, -0.00308569, -0.00366383, -0.01240032,
																						       -0.01218029, -0.0127273 , -0.00327365, -0.00231228, -0.03252913,
																								        -0.02801142, -0.00283914, -0.00283937, -0.0028439 , -0.00299275,
																										         -0.01065109, -0.01326542, -0.00453744, -0.00278599, -0.03373347,
																													       -0.03000147, -0.0023791 , -0.00370075, -0.00351622, -0.00337867,
																															        -0.01234008, -0.01382056])
"""
