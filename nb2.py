import numpy as np
import matplotlib.pyplot as plt

x=np.genfromtxt("proj_train")
xt=np.genfromtxt("proj_test")
std1=[]
m1=[]
std2=[]
m2=[]
for i in range(1,72):
	m1.append(np.mean(x[:,i][np.logical_and(~np.isnan(x[:,i]),x[:,0]==1)] ) )
	m2.append(np.mean(x[:,i][np.logical_and(~np.isnan(x[:,i]),x[:,0]==-1)] ) )
	tmp=x[:,i][np.logical_and(~np.isnan(x[:,i]),x[:,0]==1)]
	std1.append(np.std(tmp))
	tmp=x[:,i][np.logical_and(~np.isnan(x[:,i]),x[:,0]==-1)]
	std2.append(np.std(tmp))
#	tmp=x[:,i][np.logical_and(~np.isnan(x[:,i]),x[:,0]==1)]
#	tmp=np.sort(tmp)
#	std1.append(np.std(tmp[int(tmp.size*0.0):tmp.size-int(tmp.size*0.0)]))
#	tmp=x[:,i][np.logical_and(~np.isnan(x[:,i]),x[:,0]==-1)]
#	tmp=np.sort(tmp)
#	std2.append(np.std(tmp[int(tmp.size*0.0):tmp.size-int(tmp.size*0.0)]))

m1=np.array(m1)
std1=np.array(std1)
m2=np.array(m2)
std2=np.array(std2)
p=np.sum(x[:,0]==1)/float(np.sum(~np.isnan(x[:,0])))

std1=std1
std2=std2


e=0
e1=0
e2=0

y=[]
for i in range(40000):
	tmp1=np.exp(-(x[i][1:]-m1)**2/2/std1**2)/np.sqrt(2*np.pi)/std1
	tmp2=np.exp(-(x[i][1:]-m2)**2/2/std2**2)/np.sqrt(2*np.pi)/std2
	tmp1=np.product(tmp1[~np.isnan(tmp1)])
	tmp2=np.product(tmp2[~np.isnan(tmp2)])
	y.append(p*tmp1/(p*tmp1+(1-p)*tmp2))
	if p*tmp1/(p*tmp1+(1-p)*tmp2)>=0.5 and x[i][0]==-1:
		e=e+1
		e1=e1+1
	elif p*tmp1/(p*tmp1+(1-p)*tmp2)<0.5 and x[i][0]==1:
		e=e+1
		e2=e2+1


y=np.array(y)


yt=[]
for i in range(50000):
	tmp1=np.exp(-(xt[i]-m1)**2/2/std1**2)/np.sqrt(2*np.pi)/std1
	tmp2=np.exp(-(xt[i]-m2)**2/2/std2**2)/np.sqrt(2*np.pi)/std2
	tmp1=np.product(tmp1[~np.isnan(tmp1)])
	tmp2=np.product(tmp2[~np.isnan(tmp2)])
	yt.append(p*tmp1/(p*tmp1+(1-p)*tmp2))

########################################
"""
e=0
e1=0
e2=0
for i in range(40000):
	tmp1=np.exp(-(x[i][1:]-m1)**2/98/std1**2)/np.sqrt(2*np.pi)/std1/7
	tmp2=np.exp(-(x[i][1:]-m2)**2/98/std2**2)/np.sqrt(2*np.pi)/std2/7
	tmp1=np.product(tmp1[~np.isnan(tmp1)])
	tmp2=np.product(tmp2[~np.isnan(tmp2)])
	if tmp1/(p*tmp1+(1-p)*tmp2)>=0.5 and x[i][0]==-1:
		e=e+1
		e1=e1+1
	elif tmp1/(p*tmp1+(1-p)*tmp2)<0.5 and x[i][0]==1:
		e=e+1
		e2=e2+1
"""
