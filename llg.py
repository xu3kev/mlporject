import numpy as np
execfile("auc.py")

x=np.genfromtxt("proj_train")
y=x[:,0]
x=x[:,1:]
N=x.shape[0]
d=x.shape[1]
#x=np.hstack((np.ones((N,1)),x))

xt=np.genfromtxt("proj_test")
Nt=xt.shape[0]
#xt=np.hstack((np.ones((Nt,1)),xt))

ita=0.001
T=10000

#w=np.zeros(d+1)

#ty=y
#y=y.reshape(-1,1)

m1=[]
m2=[]
for i in range(x.shape[1]):
	m1.append(np.mean(x[:,i][~np.isnan(x[:,i])] ) )

for i in range(xt.shape[1]):
	m2.append(np.mean(xt[:,i][~np.isnan(xt[:,i])] ) )

m1=np.array(m1)
m2=np.array(m2)
m1=(m1*N+m2*Nt)/(N+Nt)
#y*x
#ein=[np.sum((np.sum((w*x),1)>=0)!=(ty==1))/float(N)]
#eout=[np.sum((np.sum((w*xt),1)>=0)!=(yt==1))/float(Nt)]
#ein=[]
#eout=[]
for i in range(x.shape[0]):
	for j in range(x.shape[1]):
		if np.isnan(x[i][j]):
			x[i][j]=m1[j]

for i in range(xt.shape[0]):
	for j in range(xt.shape[1]):
		if np.isnan(xt[i][j]):
			xt[i][j]=m1[j]


px=x[y==1]
nx=x[y==-1]
w=np.zeros(x.shape[1])


pp=np.random.uniform(0,10,px.shape[0])<=0.1
nn=np.random.uniform(0,10,nx.shape[0])<=0.1

px=px[pp]
nx=nx[nn]

xx=np.zeros((px.shape[0]*nx.shape[0],px.shape[1]-1))
xx=np.zeros((px.shape[0]*nx.shape[0],px.shape[1]))
yy=np.zeros(px.shape[0]*nx.shape[0])

for i in range(px.shape[0]):
	tmp=-(nx-px[i])
	xx[i*nx.shape[0]:(i+1)*nx.shape[0],:]=tmp
	#xx[i*nx.shape[0]:(i+1)*nx.shape[0],:]=(tmp[:,1::].T/tmp[:,0]).T
	yy[i*nx.shape[0]:(i+1)*nx.shape[0]]=np.sign(tmp[:,0])

ma=0

T=100
ita=0.001

for t in range(T):
	#z=(y*x)/(np.exp(np.sum(w*x,1).reshape(-1,1)*y)+1)
	i=np.random.randint(px.shape[0])
	j=np.random.randint(nx.shape[0])
	#i=random.randint(0,N-1)
	w=w+ita*(px[i]-nx[j])/(np.exp(np.dot(w,(px[i]-nx[j])))+1)
	ma2=auc(y,np.dot(x,w))
	if ma2>ma:
		ma=ma2
		wi=w

import sklearn.svm as svm

#from sklearn.linear_model import LogisticRegression as lr
#lg=lr()
