import numpy as np

def ta(x1,x2,y1,y2):
	b=abs(x1-x2)
	h=(y1+y2)/2.0
	return b*h

def auc(y,f):
	p=np.sum(y==1)
	n=np.sum(y==-1)
	aa=(np.vstack((f,y))).T
	aa=aa[aa[:,0].argsort()]
	aa=aa.T[1][::-1]
	l=np.sort(f)[::-1]
	fp=0
	tp=0
	fppr=0
	tppr=0
	a=0
	fpr=-999999999999
	i=0
	while i<l.size:
		if l[i]!=fpr:
			a=a+ta(fp,fppr,tp,tppr)
			fpr=l[i]
			fppr=fp
			tppr=tp
		if aa[i]==1:
			tp=tp+1
		else:
			fp=fp+1
		i=i+1
	a=a+ta(n,fppr,p,tppr)
	a=a/float(p*n)
	return a
